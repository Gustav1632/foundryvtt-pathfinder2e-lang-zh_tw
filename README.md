#  Pathfinder 2e in Foundry VTT 正體中文支援（廢棄）

欲使用PF2E正體中文請直接使用 
- [Translation: 正體中文 [PF2e（合集包）]](https://foundryvtt.com/packages/pf2e-compendium-zh-tw)
- [Translation: 正體中文 [PF2e]](https://foundryvtt.com/packages/pf2-cn-to-zh-tw)

如果有無法正常運作的情形，則至模組資料夾底下修改 module.json 的 zh-tw => zh-TW（暫時，後續預計全部統一為小寫）
















## 安装方法

1. 使用模组管理器貼上連結並安装
    > https://gitlab.com/Gustav1632/foundryvtt-pathfinder2e-lang-zh_tw/-/raw/master/module.json
2. 在模組管理器內啟用此模組
3. 如果角色表顯示不出來的話，直接把資料夾內的 zh-tw 替換PF2E系統資料夾的en.json，並且將 module.json裡面的翻譯引入移除。

踩坑紀錄。 3.8.2.11040 時的pf2e系統版本不知為何無法吃zh-tw的翻譯json，要直接把翻譯內容丟到en去，不然角色表會顯示不出來。

如果出現以上狀況，請重新更新SYSTEM的EN.JSON，目前推測是因為增加了新的內容，但是ZH-TW.JSON無相應內容導致的錯誤。


## 額外功能增強模組
- [PF2e GM Screen](https://foundryvtt.com/packages/PF2eGMScreen) 翻譯

## 鳴謝
**感谢純美蘋果園上的所有翻譯者**
